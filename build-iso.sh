#!/bin/sh

# Script de construction du fichier ISO 'live-build'

git pull
# lancement de la fabrication
sudo lb clean
/etc/init.d/apt-cacher-ng start
export http_proxy=http://localhost:3142/
lb config
sudo lb build
