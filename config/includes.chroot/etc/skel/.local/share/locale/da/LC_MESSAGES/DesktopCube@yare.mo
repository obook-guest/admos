��    )      d  ;   �      �     �     �  2   �     �                     "     B     O  
   ^     i  
   v     �     �  
   �     �     �     �     �     �     �                     ,  
   :     E     Q  
   ]     h     t     �     �     �     �     �     �     �     �  O  �     8     N  B   g     �     �     �     �  )   �       
     
   (     3  
   @     K     W  
   e     p     ~     �     �     �     �     �     �     �     �  
   	     	     	  
   '	     2	     >	     L	     X	     e	     t	     �	     �	     �	     �	              $                                    '           %             !                                                       (                          &      )              "   	       #       
       Animation duration Begin rotate effect Compiz Cube-like animation for workspace switching Desktop Cube Effects End rotate effect General Pullaway proportion from screen Scale effect Unscale effect easeInBack easeInBounce easeInCirc easeInCubic easeInElastic easeInExpo easeInOutBack easeInOutBounce easeInOutCirc easeInOutCubic easeInOutElastic easeInOutExpo easeInOutQuad easeInOutQuart easeInOutQuint easeInOutSine easeInQuad easeInQuart easeInQuint easeInSine easeOutBack easeOutBounce easeOutCirc easeOutCubic easeOutElastic easeOutExpo easeOutQuad easeOutQuart easeOutQuint easeOutSine Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-06-08 20:01+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da
 Animationens varighed Effekt for startrotation Compiz Terning-lignende animation ved skift mellem arbejdsområder Skrivebordsterning Effekter Effekt for slutrotation Generelt Afstand for tilbagetrækning fra skærmen Starteffekt Sluteffekt easeInBack easeInBounce easeInCirc easeInCubic easeInElastic easeInExpo easeInOutBack easeInOutBounce easeInOutCirc easeInOutCubic easeInOutElastic easeInOutExpo easeInOutQuad easeInOutQuart easeInOutQuint easeInOutSine easeInQuad easeInQuart easeInQuint easeInSine easeOutBack easeOutBounce easeOutCirc easeOutCubic easeOutElastic easeOutExpo easeOutQuad easeOutQuart easeOutQuint easeOutSine 