#!/bin/sh

# Script de configuration du bureau AdmOS
# Ce programme est lancé au démarrage du bureau, à l'aide du fichier ~/.config/autostart/admos_startup.desktop
# Auteur : obooklage (c) 2021

# 1 - Création du lien ~/www qui pointe vers /usr/var/www, autorisation de modification par l'utilisateur, permission d'accès du serveur.

sudo ln -s /var/www /home/$USER/
sudo chmod -R 777 /home/$USER/www
sudo usermod -a -G $USER www-data
sudo service apache2 restart

# 2 - création ou réglage du hostname
# L'idée est de créer un nom de machine aléatoire mais permanent si la clé est persistante
# Il permet de retrouver l'unique clé USB dans un réseau

FILE=".hostname.txt"

if [ ! -f "$FILE" ]; then
    head /dev/urandom | tr -dc A-Za-z0-9 | head -c10 > $FILE # une chaine de caractères aléatoire dans un fichier
    sudo chattr +i $FILE # Ce fichier ne peut pas être effacé
fi

sudo hostnamectl set-hostname `cat $FILE`.admos # réglage du hostname avec la chaine de caractères du fichier

# 3 - Notification dans la barre des tâches donnant l'adresse IP de la machine

sleep 20
IP=$(ip a | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' 2>/dev/null) 
zenity --notification --text="Adresse IP réseau "$IP"\nServeur web http://"$IP --title="Information réseau"
