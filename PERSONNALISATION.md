Version :  1

Personnalisation de la clé vive Freeduc-JBart
=============================================
Environnement de travail
========================
Les opérations suivantes ont été testées dans une machine virtuelle (VM) VirtualBox de type Linux Debian (64-bits) nommée 'Debian Cinnamon' dont les caractéristiques sont : mémoire 4096 Mo, 4 processeurs, affichage avec accélération vidéo 3D, disque dur 50 Gio.

Installation de Debian Cinnamon
-------------------------------
Télécharger l'image ISO [debian-live-10.2.0-amd64-cinnamon.iso](https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/) depuis le site Debian Live, la placer dans le contrôleur IDE de la VM et lancer la machine.

Installer le système d'exploitation en choisissant 'Graphical Debian Installer'.

Au démarrage, le clavier  était en qwerty. Dans le menu -> Préférences -> Clavier -> Agencements -> Ajouter  'Français (azerty)' et retirer le reste.

Pour ajouter votre utilisateur à la sudoers list : 
```console
su
sudo usermod -a -G sudo $USER
```
Puis se déconnecter et se reconnecter ou bien redémarrer.

Mise à jour
-----------
Pour avoir une machine propre et utiliser un terminal confortable :
```console
sudo apt update
sudo apt upgrade
sudo apt install gnome-terminal
```
Puis redémarrer.

Additions-Invités
-----------------

Afin de faciliter les opérations suivantes, il est vivement recommandé d'installer les Additions-Invités dans la VM. Nous pourrons partager le presse-papier, échanger les fichiers via un dossier partagé et redimensionner l'écran de la VM à la volée. Dans le menu 'Périphériques' de la machine, choisir "Insérer l'image CD des additions invités". Puis dans une console :
```console
cd /media/cdrom/
sudo sh VBoxLinuxAddition.run
sudo adduser $USER vboxsf
```
Puis redémarrer.

Installation de git et récupération du projet
=============================================
Nous utiliserons apt-cacher-ng, un proxy apt, pour limiter les re-téléchargements de paquets.
```console
sudo apt install git apt-cacher-ng live-build
git clone --branch cinnamon-jbart https://salsa.debian.org/georgesk/freeduc-jbart
cd freeduc-jbart
/etc/init.d/apt-cacher-ng start
sudo lb clean
lb config
sudo lb build
```
Le premier build est très long. En effet, tous les paquets vont être téléchargés. Ensuite, et grâce à 'apt-cacher', les builds suivants seront bien plus rapides.


Changer l'image de boot Grub2
-----------------------------

config/includes.binary/isolinux/splash.png


Changer l'image Plymouth (splash) affichée pendant le démarrage
---------------------------------------------------------------

config/includes.chroot/usr/share/plymouth/themes/futureprototype/plymouth_background_future.png  (1920x1143)

Ajouter un paquet qui est dans les dépôts
-----------------------------------------

Ajouter le nom du paquet dans le fichier config/package-lists/jbart.list.chroot.
Si des problèmes de dépendances apparaissent, procéder alors l'étape 3 de la méthode ci-dessous.

Ajouter un paquet qui n'est pas dans les dépôts
-----------------------------------------------
Par exemple le paquet ipscan_3.6.2_amd64.deb du logiciel Angry IP SCanner, site [angryip.org](https://angryip.org/)

1/ Placer le fichier *.deb dans le dossier config/config.packages.chroot

2/ Récupérer sa liste des dépendances
La méthode ci-dessous n'est sans doute pas la plus élégante... Dans un terminal : 

```console
$ sudo dpkg -i ipscan_3.6.2_amd64.deb 
(Lecture de la base de données... 277635 fichiers et répertoires déjà installés.)
Préparation du dépaquetage de ipscan_3.6.2_amd64.deb ...
Dépaquetage de ipscan (3.6.2-1) sur (3.6.2-1) ...
dpkg: des problèmes de dépendances empêchent la configuration de ipscan :
 ipscan dépend de openjdk-12-jre | openjdk-11-jre | openjdk-10-jre | oracle-java10-installer | openjdk-9-jre | oracle-java9-installer | openjdk-8-jre | oracle-java8-installer ; cependant :
  Le paquet openjdk-12-jre n'est pas installé.
  Le paquet openjdk-11-jre n'est pas installé.
  Le paquet openjdk-10-jre n'est pas installé.
  Le paquet oracle-java10-installer n'est pas installé.
  Le paquet openjdk-9-jre n'est pas installé.
  Le paquet oracle-java9-installer n'est pas installé.
  Le paquet openjdk-8-jre n'est pas installé.
  Le paquet oracle-java8-installer n'est pas installé.

dpkg: erreur de traitement du paquet ipscan (--install) :
 problèmes de dépendances - laissé non configuré
Traitement des actions différées (« triggers ») pour desktop-file-utils (0.23-4) ...
Traitement des actions différées (« triggers ») pour mime-support (3.62) ...
Des erreurs ont été rencontrées pendant l'exécution :
 ipscan
 
$ sudo apt install -f
Lecture des listes de paquets... Fait
Construction de l'arbre des dépendances
Lecture des informations d'état... Fait
Correction des dépendances... Fait
Les paquets supplémentaires suivants seront installés :
  ca-certificates-java java-common libatk-wrapper-java libatk-wrapper-java-jni libgif7 openjdk-11-jre
  openjdk-11-jre-headless
Paquets suggérés :
  default-jre fonts-wqy-microhei | fonts-wqy-zenhei fonts-indic
Les NOUVEAUX paquets suivants seront installés :
  ca-certificates-java java-common libatk-wrapper-java libatk-wrapper-java-jni libgif7 openjdk-11-jre
  openjdk-11-jre-headless
0 mis à jour, 7 nouvellement installés, 0 à enlever et 0 non mis à jour.
1 partiellement installés ou enlevés.
Il est nécessaire de prendre 37,4 Mo dans les archives.
Après cette opération, 170 Mo d'espace disque supplémentaires seront utilisés.
Souhaitez-vous continuer ? [O/n]
```

On a obtenu la liste suivante : 

```console
ca-certificates-java
java-common libatk-wrapper-java
libatk-wrapper-java-jni
libgif7 openjdk-11-jre
openjdk-11-jre-headless
```
3/ Créer un fichier /config/package-lists/ipscan_deps.list.chroot contenant cette liste des dépendances.

Changer de nom du fichier ISO
=============================

changer le nom de l'ISO qui est 'live-image-amd64.hybrid.iso' dans le fichier auto/config :
```console
   --iso-application "live-NomDelISO" \
   --iso-volume "live-NomDelISO" \
   --iso-preparer "live-NomDelISO" \
```

Utiliser la commande suivante pour mettre à jour la configuration.
```console
lb config
```

Vérifier le nom correct dans le fichier config/build
```console
[Image]
Architecture: amd64
Archive-Areas: main contrib non-free
Distribution: buster
Mirror-Bootstrap: http://deb.debian.org/debian/

[FIXME]
Configuration-Version: 1:20180925
Name: live-NomDelISO
Type: iso-hybrid

```

Modifier les applications de la barre  du tableau de bord
=========================================================
Éditer le fichier config/includes.chroot/etc/skel/.cinnamon/configs/panel-launchers@cinnamon.org/3.json

Bugs non résolus
================
En fonction de l'heure du BIOS, l'horloge du bureau n'est pas toujours à l'heure Europe/Paris.


Remerciements
=============
Merci à Georges Khaznadar, professeur de physique au lycée Jean-Bart à Dunkerque, et à la documentation originale de la page [Faire une clé vive Freeduc-jbart](https://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.fr.html).

