STICK = None

default:
	echo "possible targets: 'stick', 'rsync_to_nouveau' 'rsync_from_nouveau'"

stick:
	@if [ "$(STICK)" = "None" ]; then \
	  echo "Usage: 'make stick STICK=/dev/sdx'"; \
	else \
	  ./makeLiveStick.sh $(STICK); \
	fi


rsync_to_nouveau:
	rsync -av --exclude="live-image*" --delete auto config nouveau.freeduc.science:jbart/live-cinnamon/

rsync_from_nouveau:
	rsync -av nouveau.freeduc.science:jbart/live-cinnamon/build.log live-cinnamon/
	rsync -av --progress nouveau.freeduc.science:jbart/live-cinnamon/live-image* live-cinnamon/

.PHONY: default stick rsync_to_nouveau rsync_from_nouveau
